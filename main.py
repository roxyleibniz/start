from flask import Flask, render_template, url_for
import psycopg2
from psycopg2 import OperationalError


app = Flask(__name__)


# соединение с базой

try:
    connection = psycopg2.connect(
        database="learningactivities",
        user="postgres",
        password="0987asdf",
        host="127.0.0.1"
    )
    print("Connection to PostgreSQL DB successful")
except OperationalError as e:
    print(f"The error '{e}' occurred")

# Функции Вывода данных таблиц

# Класс

C = []
def class_Vivod():
    cursor = connection.cursor()
    postgreSQL_select_Query = "select * from class"
    cursor.execute(postgreSQL_select_Query)
    class_records = cursor.fetchall()
    for row in class_records:
        c = "[idclass]=" + str(row[0]), "[name]=" + str(row[1])
        C.append(str(c))
    return '\n'.join(C)

# Преподаватели

T = []
def teacher_Vivod():
    cursor = connection.cursor()
    postgreSQL_select_Query = "select * from teacher"
    cursor.execute(postgreSQL_select_Query)
    class_records = cursor.fetchall()
    for row in class_records:
        t = "[idteacher]=" + str(row[0]), "[fullname]=" + str(row[1]), "[qualification]=" + str(row[2])
        T.append(str(t))
    return '\n'.join(T)


# Предметы

S = []
def subject_Vivod():
    cursor = connection.cursor()
    postgreSQL_select_Query = "select * from subject"
    cursor.execute(postgreSQL_select_Query)
    class_records = cursor.fetchall()
    for row in class_records:
        s = "[idsubject]=" + str(row[0]), "[name]=" + str(row[1])
        S.append(str(s))
    return '\n'.join(S)


# Руты и пути для сайта

@app.route('/')
def index():
    return render_template("index.html")


@app.route('/teacher')
def teacher():
    teacher = teacher_Vivod()
    return render_template("teacher.html", teacher=teacher)


@app.route('/class')
def class1():
    clas = class_Vivod()
    return render_template("class.html", clas=clas)


@app.route('/subject')
def subject():
    subj = subject_Vivod()
    return render_template("subject.html", subj=subj)


@app.route('/user/<string:name>/<int:id>')
def user(name, id):
    return "User page: " + name + " " + str(id)

if __name__ == "__main__":
    app.run(debug=True)


